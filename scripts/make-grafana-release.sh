#!/bin/sh

set -e

AUTOMATION=${1:-no}

set -u

get_latest_grafana_release() {
    curl -s -L --fail "https://hub.docker.com/v2/repositories/grafana/grafana/tags/?page_size=1000" | \
        jq '.results | .[] | .name' -r | \
        egrep '^[0-9]\.[0-9]\.[0-9]$' | \
        sort -nr | \
        head -n1
}

get_latest_local_release() {
    git tag | sort -nr | head -n1
}

make_release() {
	VERSION="${OFFICIAL}" envsubst < Dockerfile.tmpl > Dockerfile
	git add Dockerfile
	git commit -m "Updated grafana to ${OFFICIAL}"
	git tag ${OFFICIAL}
	git push
	git push --tags
}

OFFICIAL="$(get_latest_grafana_release)"
LOCAL="$(get_latest_local_release)"

if [ "${OFFICIAL}" = "${LOCAL}" ]; then
    echo "No updates, both versions at ${OFFICIAL}"
    exit 0
fi

if [ "${AUTOMATION}" != "yes" ]; then
    while true; do
        read -p "Build release for Grafana ${OFFICIAL}? " answer
        case $answer in
            [Yy]* ) make_release; break;;
            [Nn]* ) exit 0;;
            * )     echo "Please answer yes or no.";;
        esac
    done
elif [ "${AUTOMATION}" = "yes" ];  then
    make_release
fi
