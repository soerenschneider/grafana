#!/bin/sh

set -e

AUTOMATION=${1:-no}

set -u

make_release() {
    TAG=$(date +%Y%m%d%H%M%S)
    git add -A
    git commit -m "Update to tag ${TAG}"
    git tag ${TAG}
    git push
    git push --tags
}

if [ "${AUTOMATION}" != "yes" ]; then
    CHANGES_NUM=$(git status $(git rev-parse --show-toplevel)/dashboards --porcelain=v1 | wc -l)
    if [ ${CHANGES_NUM} -eq 0 ]; then
        echo "No changes in the dashboards folder"
    fi

    while true; do
        git status $(git rev-parse --show-toplevel)/dashboards --porcelain=v1
        read -p "Build release for Grafana? " answer
        case $answer in
            [Yy]* ) make_release; break;;
            [Nn]* ) exit 0;;
            * )     echo "Please answer yes or no.";;
        esac
    done

elif [ "${AUTOMATION}" = "yes" ];  then
    make_release
fi
